  let notRunning = true;

  function getRandomColor() {

    let letters = '0123456789ABCDEF'.split('');
    let color  = '#';

    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }

    return color;
  }


  function discoSquare() {

     let canvas = document.getElementById("whiteBoard");
     let context = canvas.getContext('2d');


     let verPos = Math.floor((Math.random() * 399) + 1);
     let theColor = getRandomColor();

     context.lineWidth = 10;
     context.strokeStyle = theColor;
     context.moveTo(10,verPos);
     console.log("vertical pos: " + verPos);

     context.lineTo(400,verPos);
     context.lineCap = "butt";
     context.stroke();

     document.getElementById('displayColor').innerHTML  = theColor;

  }

  let animation;


  document.getElementById("canvasStart").onclick = function() {

    if(notRunning) {

        animation = setInterval(discoSquare,100);
        notRunning = false;
    }else{
        alert('already running!!!');
    }
  }

  document.getElementById("canvasPause").onclick = function() {
    
    clearInterval(animation);
    notRunning = true;
  }